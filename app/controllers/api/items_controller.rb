require 'json'

class Api::ItemsController < ApplicationController

  def index
    page = params[:page].to_i
    items_per_page = params[:per_page].to_i
    num_items = Item.all().length
    num_pages = num_items / items_per_page
    if num_items > num_items / items_per_page
      num_pages = num_pages + 1
    end
    items = Item.page(page).per(items_per_page)
    render json: {items: items, meta: {total_pages: num_pages}}
  end

end
