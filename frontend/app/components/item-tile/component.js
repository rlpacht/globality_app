import Ember from 'ember';

const ItemTileComponent = Ember.Component.extend({
  classNames: ['item-result'],

  item: null,

  // displayedBlurb: this.get('item').blurb,

  truncatedBlurb: Ember.computed(function() {
    var itemBlurb = this.get('item.blurb');
    var blurbWords = itemBlurb.split(" ");
    var subBlurbWords = blurbWords.slice(0,32);
    return `${subBlurbWords.join(" ")}...`;
  })
});

export default ItemTileComponent;