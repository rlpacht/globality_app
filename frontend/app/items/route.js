import Ember from 'ember';
import InfinityRoute from "ember-infinity/mixins/route";

const ItemsRoute = Ember.Route.extend(InfinityRoute, {
  model() {
    return this.infinityModel("item", {perPage: 20, startingPage: 1 });
  }

});

export default ItemsRoute;
