import Ember from 'ember';

const ItemsController = Ember.Controller.extend({
  items: Ember.computed.alias('model')
});

export default ItemsController;