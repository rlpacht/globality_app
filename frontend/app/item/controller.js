import Ember from 'ember';

const ItemController = Ember.Controller.extend({
  item: Ember.computed.alias("model")
});

export default ItemController;