import Ember from 'ember';

const ItemRoute = Ember.Route.extend({
  model(params) {
    const id = params.id;
    return this.store.findRecord('recipe', id);
  }
});

export default ItemRoute;