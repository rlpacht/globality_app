import DS from 'ember-data';

const ItemModel = DS.Model.extend({
  title: DS.attr("string"),
  blurb: DS.attr("string"),
  author: DS.attr("string"),
  thumbnail_url: DS.attr("string"),
  details_url: DS.attr("string")
});

export default ItemModel;