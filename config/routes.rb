Rails.application.routes.draw do
  root "application#index"

  namespace :api do
    resources :items
  end
end
