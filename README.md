To run locally
 * run bundle install,
 * rake db:create
 * rake db:migrate
 * rake db:seed

 then run rails s and go to http://localhost:3000/ to see the app.

 The live version can be found [here](https://quiet-depths-2150.herokuapp.com/)
