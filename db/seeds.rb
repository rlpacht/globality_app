  # This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

60.times do |i|
  items = Item.create({
    title: Faker::Lorem.sentence(2),
    blurb: Faker::Lorem.sentence(44),
    author: Faker::Name.name,
    thumbnail_url: "http://assets.aarp.org/www.aarp.org_/cs/fun/nancymullrich.jpg",
    details_url: Faker::Internet.url('example.com')
    })
end
