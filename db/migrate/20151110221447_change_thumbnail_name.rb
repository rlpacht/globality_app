class ChangeThumbnailName < ActiveRecord::Migration
  def change
    rename_column :items, :thumbnail, :thumbnail_url
  end
end
